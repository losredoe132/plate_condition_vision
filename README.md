# plate_condition_vision
Computer vision to detect if a build plate in the empty plate storage is polluted or clean. 
Taking and analyising an image is triggered by an inductive proximity switch which is connected to the pins specified in ```/plate_condition_vision/pi_pins.py```. Additionally a proximity switch can be used to check if the full plate storage is full. 
All information are available over a flask based REST-API with three endpoints : ```/input_storage_statee```, ```/input_storage_image```, ```/update_reference``` and ```/output_storage_state```. 

## Interfaces and wiring
Plug the basler camera into one of the two USB3 ports of the raspberry pi. The correct port is detected automatically. The inductive proximity switches need 24V supply voltage but the rapsberry pi is working with 3.3V logic level. To convert a voltage divider, as shown in [link](http://www.3d-proto.de/public/img/IndSensorSetup.png) is needed. In contrast to this example we need other resistors, due to the different supply voltages. 220k and 33k should work fine.

## Installation 
Clone the repo into your workspace. cd into it. Than execute:
```
source .venv/bin/activate
pip install -r ./requirements.txt
python3 ./plate_condition_vision/flask_api.py
```
There is also a shell script to execute those steps. Make it executable (```chmod +x run_flask.sh```) and run it with: ```./run_flask.sh```. 

## Deployment 
It is deployed in form of two different systemctl-services: 
- run_api.sh
- run_observer.sh

Booth are deployed independently as shown in the ```plate_storage_observer.service```file. 

## Usage 
The REST API hosted by Flask should be available on http://localhost:5000 (as used in ARPS due to edge router: http://141.70.213.153:24500) with endpoints: 
- /input_plate_storage_state (json)
- /input_plate_storage_image (image)
- /output_plate_storage_state (json)
- /update_reference (json)

accessible with GET-HTTP methods as shown in the curl requests in folder curl/* .

## Example 
The image (lower right picture) is croped into different images which are analysed independently. 
In the red and green area multiple steps of smoothing, subtracting and thresholding is executed to detect filament. 
![example_analysis](img_example/analysis.png)
Its imporant that:
- a valid integer datamatrix is in the yellow rectangle area. 
- there is a reference image of the corresponding plate in the img_pattern/ folder. If not call the update_reference/ endpoint.

## Get Data to local 
Execute the src/
Download ziped data: 
```
sftp -P 24022 pi@141.70.213.153
get /home/pi/plate_condition_vision_image_storage/data_compressed.zip /Users/finnjonaspeper/Downloads
```