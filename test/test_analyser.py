from plate_condition_vision.analyser import PlateConditionAnalyser


class TestAnalyserMethods():

    def test_bed_example(self):
        result = PlateConditionAnalyser(
            "img_pattern/ref_3.png").analyse()
        assert result["holes_correct"] == 'True'
        assert result["surface_correct"] == 'True'
        assert result["pollution_bed"] < 0.001
        assert result["pollution_ncs"] < 0.001
        assert result["id"] == 3

    # Bed
    def test_clean_positive(self):
        # TODO
        pass

    def test_clean_negative(self):
        # TODO
        pass

    # NCS
    def test_ncs_positive(self):
        # TODO
        pass

    def test_ncs_negative(self):
        # TODO
        pass

    # Plate present
    def test_plate_present_positive(self):
        # TODO
        pass

    def test_plate_present_negative(self):
        # TODO
        pass

    # Surface correct
    def test_surface_positive(self):
        # TODO
        pass

    def test_surface_negative(self):
        # TODO
        pass

    # Hole
    def test_holes_positive(self):
        # TODO
        pass

    def test_holes_negative(self):
        # TODO
        pass
