import logging
import os
import time

import toml
import cv2  # type: ignore
import matplotlib.pyplot as plt  # type: ignore
import numpy as np  # type: ignore
from pylibdmtx.pylibdmtx import decode  # type: ignore
from typing_extensions import TypedDict
from typing import List, Tuple
import argparse


class AnalysisJSON(TypedDict):
    holes_correct: str
    surface_correct: str
    pollution_bed: float
    pollution_ncs: float
    timestamp: int
    duration: int
    id: int
    msg: str


class PlateConditionAnalyser():
    '''provides access to loading, preprocessing and analyzing images.
    '''

    def __init__(self, source, timestamp: int = None) -> None:
        '''depening on the type of source, load the image or assign it
        its possible to give the constructor a user defined timestamp. if none is given, the
        timestamp will be automatically defined

        Parameters
        ----------
        source : str or np.ndarray
            Source of the image. if source is a string it is handled as a path to the image.
            If it is a np.ndarray it is handled as the image itself.
        timestamp : int, optional
            Externaly defiend timestamp. If None is given, the timestamp will be automatically
            defined, by default None

        Raises
        ------
        TypeError
            If the tpye of the loaded image is None (if the image is not existent or unreadable)
        '''
        # load config
        with open("config/config.toml") as fh:
            self.config = toml.load(fh)

        self.pin_inductive_switch_input = int(self.config["Pins"]["pin_inductive_switch_input"])

        self.msg: str = ""

        self.path_visualization: str = os.path.join(os.getcwd(),
                                                    "img/analysis.png")
        self.paths_reference: str = os.path.join(os.getcwd(),
                                                 "img_pattern/")
        self.path_hole_pattern: str = os.path.join(os.getcwd(),
                                                   "img_pattern/pattern_hole.png")

        if timestamp is not None:
            self.timestamp = int(timestamp)
            logging.info("id defined by user")
        else:
            self.timestamp = int(time.time())
            logging.info("no id given. id defined by int(time.time())")

        # init analysis attributes
        self.holes_correct: bool = False
        self.surface_correct: bool = False
        self.pollution_bed: float = 0
        self.pollution_ncs: float = 0
        self.id: int = 0

        # visulaization properties
        self.thickness_lines = 2

        # kernel size for smoothing
        self.kernel_size = 8

        # minimum jump in surface detectiob to be detected
        self.surface_threshold = 3
        # maximum value to detect pollution
        self.threshold_pollution_bed = 15
        self.threshold_pollution_ncs = 2
        # minum value to accept hole posiiton
        self.matching_score_threshold = 0.8

        # load ref image
        hole_pattern = self.load_image(self.path_hole_pattern)
        self.hole_pattern = cv2.cvtColor(hole_pattern, cv2.COLOR_RGB2GRAY)

        # load image to be analized
        if isinstance(source, str):
            self.img = self.load_image(source)
        elif isinstance(source, np.ndarray):
            self.img = source
        else:
            self.msg = "The image is not found. Check the permissions and paths"
            raise TypeError('''PlateConditionAnalyser excepts only
            np.ndarray (the image itself) or str (path to image)''')

        # copy original image for annotation
        self.img_annotated = self.img.copy()

    def load_image(self, path: str) -> np.ndarray:
        '''load an image and return it

        Parameters
        ----------
        path : str
            the path where the image is located

        Returns
        -------
        np.ndarray
            NxMx3 np.ndarray representing the image

        Raises
        ------
        TypeError
            if path is not valid or cannot be found
        '''
        logging.debug("load image")
        img = cv2.imread(path)

        if img is None:
            raise TypeError(
                f"File {path} not found! Cant operate on None types.")

        return img

    def analyse_holes_correct(self, img: np.ndarray) -> bool:
        '''analyse if the orientation of the plate is correct

        Parameters
        ----------
        img : np.ndarray
            image

        Returns
        -------
        bool
            indicator: if true, the orientation of the plate regarding the holes is correct
        '''
        logging.debug("analyse_holes_correct")
        config_holes = self.config["rois"]["holes_correct"]

        img_right = img[config_holes["start_right"][1]:config_holes["end_right"][1],
                        config_holes["start_right"][0]:config_holes["end_right"][0]]
        img_left = img[config_holes["start_left"][1]:config_holes["end_left"][1],
                       config_holes["start_left"][0]:config_holes["end_left"][0]]

        self.img_annotated = cv2.rectangle(self.img_annotated,
                                           config_holes["start_right"],
                                           config_holes["end_right"],
                                           (128, 255, 255),
                                           self.thickness_lines)

        self.img_annotated = cv2.rectangle(self.img_annotated,
                                           config_holes["start_left"],
                                           config_holes["end_left"],
                                           (128, 255, 255),
                                           self.thickness_lines)

        method = cv2.TM_CCOEFF_NORMED
        self.matching_map_right = cv2.matchTemplate(img_right, self.hole_pattern, method)
        self.matching_map_left = cv2.matchTemplate(img_left, self.hole_pattern, method)

        self.matching_map_combined = np.append(self.matching_map_right, self.matching_map_left, axis=0)
        # self.matching_map_annotated = self.matching_map.copy()

        # take the 10th greatest value to avoid outliers
        self.matching_score_right = np.sort(self.matching_map_right.flatten())[-10]
        self.matching_score_left = np.sort(self.matching_map_left.flatten())[-10]

        matching_right = self.matching_score_right > self.matching_score_threshold
        matching_left = self.matching_score_left > self.matching_score_threshold

        # if there is a maximum value greater than threshold nearby desired hole position
        if matching_left or matching_right:
            return True
        return False

    def analyse_surface_correct(self, img: np.ndarray) -> bool:
        '''Analyse if the build plate is upside down by checking the characterstic edge

        Parameters
        ----------
        img : np.ndarray
            image to be analysed

        Returns
        -------
        bool
            indicator: if true, the surface is detected as correct
        '''
        logging.debug("analyse_surface_correct")
        img = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
        self.foil_edge_vals = np.mean(img, axis=0)
        self.foil_edge_diffs = np.diff(self.foil_edge_vals)
        self.foil_edge_jump = np.max(np.abs(self.foil_edge_diffs))
        return self.foil_edge_jump > self.surface_threshold

    def analyse_pollution(self, img_bed: np.ndarray, ref_bed: np.ndarray, threshold: int):
        '''Analye pollution and return pollution_map as array and pollution
        as float.

        Parameters
        ----------
        img_bed : np.ndarray
            image to be analysed.
        ref_bed : np.ndarray
            Reference image, must have the same dimension as img_bed

        Returns
        -------
        (pollution: float, pollution_map: np.ndarray)
            pollution as ratio polluted pixel over image pixel number,
            pollution_map is a img_bed sized array where polluted pixels
            are 1 and unpolluted pixels are 0.
        '''
        logging.debug("analyse_pollution")

        # calculate absolute pixelwise difference
        diff_abs = np.abs(np.subtract(img_bed.astype(int), ref_bed.astype(int))).astype("uint8")

        # thresholding
        pollution_map: np.ndarray = cv2.threshold(
            diff_abs, threshold, 1, cv2.THRESH_BINARY)[1]

        if False:
            _, axs = plt.subplots(2, 2, figsize=(10, 10))
            cmap = "gray"
            cv2.normalize(img_bed, img_bed, 0, 255, cv2.NORM_MINMAX)
            cv2.normalize(ref_bed, ref_bed, 0, 255, cv2.NORM_MINMAX)

            h = axs[0, 0].imshow(img_bed, cmap=cmap, vmin=0, vmax=255)
            plt.colorbar(h)
            h = axs[0, 1].imshow(ref_bed, cmap=cmap, vmin=0, vmax=255)
            h = axs[1, 0].imshow(diff_abs, cmap=cmap, vmin=0, vmax=255)
            h = axs[1, 1].imshow(pollution_map, cmap=cmap)
            plt.savefig(f"img/analysis_{int(time.time())}.png")

        pollution: float = np.mean(pollution_map)
        return pollution, pollution_map

    def analyse_datamatrix(self, img_datamatrix: np.ndarray) -> int:
        '''Analyse datamatrix. Only integer values are allowd as datamatrix value.

        Parameters
        ----------
        img_datamatrix : np.ndarray
            imgage of the datamatrix

        Returns
        -------
        int
            id of the datamatrix
        '''
        logging.debug("analyse_datamatrix")

        datamatrix_data = decode(img_datamatrix,
                                 max_count=1,
                                 max_edge=60)
        if len(datamatrix_data) == 1:
            datamatrix_decoded = datamatrix_data[0].data.decode('UTF-8')
            if datamatrix_decoded.isdigit():
                id = int(datamatrix_decoded)
            else:
                id = 0
        elif len(datamatrix_data) == 0:
            id = 0
            logging.info("found no valid data matrix")
        else:
            id = 0
            logging.info("found more than one data matrix")
        return id

    def visualize_analysis(self, *path) -> None:
        '''visualize the anaylsis and save it in path
        '''

        # if path is given overwrite default save path
        if path:
            self.path_visualization = path[0]

        # visulization
        fig, axs = plt.subplots(3, 2, figsize=(20, 20))

        # Bed
        axs[0, 0].set_title(f"Bed Pollution: {self.pollution_bed:.4f}")
        axs[0, 0].imshow(self.pollution_map_bed, cmap="Reds", vmin=0, vmax=1)

        # Datamatrix
        axs[1, 0].set_title(f"datamatrix: {self.id}")
        axs[1, 0].imshow(self.img_datamatrix, cmap="gray")

        # Sections
        axs[1, 1].set_title(f"shape: {self.img_annotated.shape}")
        axs[1, 1].imshow(self.img_annotated, cmap="gray")

        # foil detection
        axs[2, 0].set_title(f"max foil edge jump: {self.foil_edge_jump:.4f}")
        axs[2, 0].plot(self.foil_edge_vals, c="b")
        scnd_axs = axs[2, 0].twinx()
        scnd_axs.plot(self.foil_edge_diffs, c="g")

        # matching maps
        axs[2, 1].set_title(
            f"right: {self.matching_score_right:.2f}, left: {self.matching_score_left:.2f}")

        # plt.tight_layout()
        fig.savefig(self.path_visualization)

    def crop_image(self,
                   img: np.ndarray,
                   start_point: List,
                   end_point: List,
                   color: Tuple[int, int, int]) -> np.ndarray:
        '''crop the image and return it. Mark the cropped area in the img_annotated object attribute

        Parameters
        ----------
        img : np.ndarray
            image to be cropped
        start_point : List
            start point from the crop rectangel
        end_point : List
            end point from the crop rectangel
        color : List
            color for the marker as a List of RGB values, e.g. [128,255,0]

        Returns
        -------
        np.ndarray
            cropped image
        '''
        logging.debug("crop image")
        self.img_annotated = cv2.rectangle(self.img_annotated,
                                           start_point,
                                           end_point,
                                           color,
                                           self.thickness_lines)

        return img[start_point[1]:end_point[1],
                   start_point[0]:end_point[0]]

    def preprocess_img(self, img: np.ndarray) -> np.ndarray:
        '''preprocess image by greyscale and gaussian smoothing

        Parameters
        ----------
        img : np.ndarray
            image to be processed

        Returns
        -------
        np.ndarray
            greyscaled and smoothed image
        '''
        kernel = np.ones((self.kernel_size, self.kernel_size), np.float32)/self.kernel_size**2
        img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        img = cv2.filter2D(img, -1, kernel)
        return img

    def analyse(self,) -> AnalysisJSON:
        '''pipeline to analyse the image and return a dictonary which contains the analysed data

        Returns
        -------
        AnalysisJSON
            "holes_correct": str(self.holes_correct),
            "surface_correct": str(self.surface_correct),
            "pollution_bed": float(self.pollution_bed),
            "pollution_ncs": float(self.pollution_ncs),
            "timestamp": int(self.timestamp),
            "duration": int(self.duration),
            "id": int(self.id)}
            "msg": optional msg for Exception handling. If nothing mentionable happend,
                   it evaluates to "[]"
        '''
        t_0 = time.time()

        # preprocess img
        self.img_preprocessed = self.preprocess_img(self.img)

        # analyse datamatrix
        self.img_datamatrix = self.crop_image(
            self.img,
            self.config["rois"]["datamatrix"]["start"],
            self.config["rois"]["datamatrix"]["end"],
            self.config["rois"]["datamatrix"]["color"])
        self.img_datamatrix = cv2.normalize(
            self.img_datamatrix, self.img_datamatrix, 0, 255, cv2.NORM_MINMAX)
        self.id = self.analyse_datamatrix(self.img_datamatrix)

        # analyse bools
        self.holes_correct = self.analyse_holes_correct(self.img_preprocessed)

        img_surface_correct: np.ndarray = self.crop_image(
            self.img,
            self.config["rois"]["surface_correct"]["start"],
            self.config["rois"]["surface_correct"]["end"],
            self.config["rois"]["surface_correct"]["color"])
        self.surface_correct = self.analyse_surface_correct(img_surface_correct)

        # load ref image
        ref_filename: str = "ref_" + str(self.id) + ".png"
        if ref_filename in os.listdir("img_pattern"):
            self.ref = self.load_image(self.paths_reference + ref_filename)
        else:
            self.ref = self.load_image(self.paths_reference + "ref_0.png")
            self.msg = "no_ref"
        self.ref_preprocessed = self.preprocess_img(self.ref)

        # analyse pollution
        # area of interest: print bed
        img_bed: np.ndarray = self.crop_image(
            self.img_preprocessed,
            self.config["rois"]["pollution_bed"]["start"],
            self.config["rois"]["pollution_bed"]["end"],
            self.config["rois"]["pollution_bed"]["color"])
        ref_bed: np.ndarray = self.crop_image(
            self.ref_preprocessed,
            self.config["rois"]["pollution_bed"]["start"],
            self.config["rois"]["pollution_bed"]["end"],
            self.config["rois"]["pollution_bed"]["color"])
        self.pollution_bed, self.pollution_map_bed = self.analyse_pollution(
            img_bed, ref_bed, self.threshold_pollution_bed)

        self.duration = (time.time()-t_0)*1000

        analysis: AnalysisJSON = {"holes_correct": str(self.holes_correct),
                                  "surface_correct":  str(self.surface_correct),
                                  "pollution_bed": float(np.round(self.pollution_bed, 4)),
                                  "pollution_ncs": float(0),
                                  "timestamp": int(self.timestamp),
                                  "duration": int(self.duration),
                                  "id": int(self.id),
                                  "msg": str(self.msg)}

        logging.info(analysis)

        return analysis


if __name__ == "__main__":
    logging.basicConfig(filename='analysis.log',  level=logging.DEBUG)
    parser = argparse.ArgumentParser()
    parser.add_argument("--path", help="path of the image")
    args = parser.parse_args()
    if args.path:
        path = args.path
    else:
        path = "img/1648016056.png"

    pca = PlateConditionAnalyser(path)
    analysis = pca.analyse()
    print(analysis)
    pca.visualize_analysis()
