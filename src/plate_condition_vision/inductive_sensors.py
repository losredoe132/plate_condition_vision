import logging
import RPi.GPIO as GPIO  # type: ignore


def check_gpio_pin_deprecated(gpio_pin: int) -> int:
    """return the state of the inductive proximity at the full plate storage switch
    as string representing a bool """

    # setup pin
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(gpio_pin, GPIO.IN)

    # read sensor state
    state: int = GPIO.input(gpio_pin)

    GPIO.cleanup()

    logging.info(f"storage_state @ pin {gpio_pin} {state}")
    return state
