import os
import pickle

import cv2  # type: ignore
import matplotlib.pyplot as plt  # type: ignore
import numpy as np  # type: ignore


def crop_hole_pattern(img):
    pattern = img[5:75, 1665:1712]
    # pattern = img
    kernel_size = 2
    kernel = np.ones((kernel_size, kernel_size), np.float32)/kernel_size**2
    img = cv2.cvtColor(pattern, cv2.COLOR_BGR2GRAY)
    img = cv2.filter2D(img, -1, kernel)
    cv2.imwrite("img_pattern/pattern_hole.png", img)


def find_max_pos(matching_map):
    indexs = np.argmax(matching_map, axis=None)
    pos_max = np.unravel_index(indexs, res.shape)
    return pos_max


path = "img_pattern/reference.png"
img = cv2.imread(path)


path_hole_pattern: str = os.path.join(os.getcwd(),
                                      "img_pattern/pattern_hole.png")
template = cv2.imread(path_hole_pattern)
template = cv2.cvtColor(template, cv2.COLOR_BGR2GRAY)

# undistort
with open("config/mtx.p", "rb") as fh:
    mtx = pickle.load(fh)

with open("config/dist.p", "rb") as fh:
    dist = pickle.load(fh)

h,  w = img.shape[:2]
newcameramtx, roi = cv2.getOptimalNewCameraMatrix(
    mtx, dist, (w, h), 1, (w, h))
dst = cv2.undistort(img, mtx, dist, None, newcameramtx)
x, y, w, h = roi
dst = dst[y:y+h, x:x+w]

# crop_hole_pattern(dst)


img_raw = dst
img = img_raw.copy()

kernel_size = 12
kernel = np.ones((kernel_size, kernel_size), np.float32)/kernel_size**2
img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
img = cv2.filter2D(img, -1, kernel)

cut_offset_x = 1600
cut_offset_y = 400

img_search = img[:, cut_offset_x:]
method = cv2.TM_CCOEFF_NORMED
res = cv2.matchTemplate(img_search, template, method)


res_search_1 = res[:cut_offset_y, :]
print(find_max_pos(res_search_1))
res_search_2 = res[cut_offset_y:, :]
print(find_max_pos(res_search_2))

fig, axs = plt.subplots(3, 1, figsize=(5, 10))
axs[0].imshow(img_raw, cmap="gray")
axs[1].imshow(template, cmap="gray")
h = axs[2].imshow(res, cmap='viridis')

plt.colorbar(h)
plt.savefig("holes_correct.png")
