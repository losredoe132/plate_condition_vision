import numpy as np
import cv2
from plate_condition_vision.analyser import PlateConditionAnalyser
import shutil
import os


def downscale_save(path_source: str, path_target: str):
    '''downscale the image defined in path_source and save it to path_target

    Parameters
    ----------
    path_source : str
        source path
    path_target : str
        target path
    '''

    pca = PlateConditionAnalyser(path_source)
    img: np.ndarray = pca.crop_image(
        img=pca.img,
        start_point=pca.config["rois"]["pollution_bed"]["start"],
        end_point=pca.config["rois"]["pollution_bed"]["end"],
        color=pca.config["rois"]["pollution_bed"]["color"])

    width = 256
    height = 256
    dim = (width, height)
    resized = cv2.resize(img, dim, interpolation=cv2.INTER_AREA)
    cv2.imwrite(path_target, resized)
    print(f"downscaled {path_source} successfully")


def process_compress(path_source_dir, path_target_dir, path_archive):
    already_downscaled_images = os.listdir(path_target_dir)

    for i, filename in enumerate(os.listdir(path_source_dir)):
        print(f"{i+1:4}/{len(os.listdir(path_source_dir)):4}     ", end="")
        if filename not in already_downscaled_images:
            path_source = os.path.join(path_source_dir, filename)
            path_target = os.path.join(path_target_dir, filename)
            downscale_save(path_source, path_target)
        else:
            print(f"{filename:15} is already downscaled.")

    print("compress...")
    shutil.make_archive(path_archive, 'zip', path_target_dir)
    print("finished.")


if __name__ == "__main__":
    path_source_dir = "/home/pi/plate_condition_vision_image_storage/raw"
    path_target_dir = "/home/pi/plate_condition_vision_image_storage/downscaled"
    path_archive = os.path.join("/home/pi/plate_condition_vision_image_storage", "data_compressed")

    process_compress(path_source_dir, path_target_dir, path_archive)
