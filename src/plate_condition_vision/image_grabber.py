'''
A simple Program for grabing video from basler camera and converting it to
opencv img. Tested on Basler acA1300-200uc (USB3, linux 64bit , python 3.5)
'''
import logging
import os
import time
import toml

import cv2  # type: ignore
import numpy as np  # type: ignore
from pypylon import pylon  # type: ignore


class ImageGrabber():

    def __init__(self) -> None:
        # conecting to the first available camera
        self.camera = pylon.InstantCamera(
            pylon.TlFactory.GetInstance().CreateFirstDevice())

    def load_config_cam(self, path: str) -> None:
        # load the config file *.pfs specified in path to the camera:
        # https://github.com/basler/pypylon/blob/master/samples/parametrizecameraloadandsaveconfig.py

        self.camera.Open()
        logging.info(
            f"Using device {self.camera.GetDeviceInfo().GetModelName()}")
        logging.info(f"Loading file {path} on camera.")
        try:
            pylon.FeaturePersistence.Load(path, self.camera.GetNodeMap(), True)
        except Exception as e:
            logging.error(e)
        # Close the camera.
        self.camera.Close()

    def save_config_cam(self, path: str) -> None:
        # save the config file from the camera to file [path].pfs:
        # https://github.com/basler/pypylon/blob/master/samples/parametrizecameraloadandsaveconfig.py

        self.camera.Open()
        logging.info(
            f"Using device {self.camera.GetDeviceInfo().GetModelName()}")
        logging.info(f"Saving camera's node map to file... {path}")
        # Save the content of the camera's node map into the file.
        pylon.FeaturePersistence.Save(path, self.camera.GetNodeMap())
        self.camera.Close()

    def take_picture(self, timestamp: int = None) -> np.ndarray:

        if timestamp is not None:
            self.timestamp = int(timestamp)
            logging.info(f"id defined by user: {self.timestamp}")
        else:
            self.timestamp = int(time.time())
            logging.info("no id given. id defined by int(time.time())")

        self.camera.Open()

        numberOfImagesToGrab: int = 1
        self.camera.StartGrabbingMax(numberOfImagesToGrab)
        converter = pylon.ImageFormatConverter()

        # converting to opencv bgr format
        converter.OutputPixelFormat = pylon.PixelType_RGB8packed
        converter.OutputBitAlignment = pylon.OutputBitAlignment_MsbAligned

        while self.camera.IsGrabbing():
            grabResult = self.camera.RetrieveResult(
                5000, pylon.TimeoutHandling_ThrowException)

        if grabResult.GrabSucceeded():
            # Access the image data
            image = converter.Convert(grabResult)
            img = image.GetArray()

            self.save_path: str = os.path.join(
                os.getcwd(), f"img/{self.timestamp}.png")
            cv2.imwrite(self.save_path, img)
            logging.info(f"SUCCESS! Image saved in {self.save_path}")

        grabResult.Release()

        # Releasing the resource
        self.camera.Close()

        return img


if __name__ == "__main__":
    os.chdir("/home/pi/plate_condition_vision")
    with open("config/config.toml") as fh:
        config = toml.load(fh)
    ig = ImageGrabber()
    path_config_cam: str = os.path.join(os.getcwd(), config["Camera"]["config_path"])

    ig.load_config_cam(path_config_cam)
    img = ig.take_picture(timestamp=0)
