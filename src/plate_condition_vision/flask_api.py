import time
import logging
import os
import cv2  # type: ignore
import toml

from flask import Flask, send_file, request  # type: ignore
from flask.logging import default_handler  # type: ignore

from plate_condition_vision.analyser import PlateConditionAnalyser
from plate_condition_vision.image_grabber import ImageGrabber
from plate_condition_vision.update_reference import update_reference
from plate_condition_vision.image_downscaler import process_compress

os.chdir("/home/pi/plate_condition_vision")
with open("config/config.toml") as fh:
    config = toml.load(fh)

pin_inductive_switch_output: int = int(config["Pins"]["pin_inductive_switch_output"])
pin_inductive_switch_input: int = int(config["Pins"]["pin_inductive_switch_input"])

app = Flask(__name__)
app.logger.removeHandler(default_handler)

path_logs: str = 'logs/flask_api.log'
logging.basicConfig(filename=path_logs,
                    format='%(asctime)s %(levelname)-8s %(message)s',
                    datefmt='%Y-%m-%d %H:%M:%S',
                    level=logging.INFO)

ig = ImageGrabber()
path_config_cam: str = os.path.join(os.getcwd(), config["Camera"]["config_path"])
ig.load_config_cam(path_config_cam)


@app.route("/input_storage_state", methods=['GET'])
def get_input_plate_storage_state():
    timestamp = int(time.time())
    img = ig.take_picture(timestamp)

    pca = PlateConditionAnalyser(img, timestamp=timestamp)
    analysis = pca.analyse()

    try:
        return analysis
    except Exception as e:
        return str(e)


@app.route("/input_storage_image", methods=['GET'])
def get_input_plate_storage_image():

    content = request.get_json()

    requested_image_ts = content['timestamp']
    requested_image_path = f"img/{requested_image_ts}.png"
    try:
        requested_image = cv2.imread(requested_image_path)
    except Exception as e:
        return e

    pca = PlateConditionAnalyser(requested_image)
    pca.analyse()
    pca.visualize_analysis()

    try:
        return send_file("../../img/analysis.png",
                         download_name='input_plate_storage_img.png')
    except Exception as e:
        return str(e)


@app.route("/update_reference", methods=['GET'])
def get_update_reference():
    n_imgs = 2

    content = request.get_json()
    if content is not None:
        if "n_imgs" in content.keys():
            n_imgs = content["n_imgs"]
        if "include_old" in content.keys():
            include_old: bool = bool(content["include_old"])
        else:
            include_old: bool = True

        report = update_reference(n_imgs, include_old=include_old)

    return report


@app.route("/get_all_images", methods=['GET'])
def get_all_images():
    path_source_dir = "/home/pi/plate_condition_vision_image_storage/raw"
    path_target_dir = "/home/pi/plate_condition_vision_image_storage/downscaled"
    path_archive = os.path.join("/home/pi/plate_condition_vision_image_storage", "data_compressed.zip")

    process_compress(path_source_dir, path_target_dir, path_archive)
    try:
        return send_file(path_archive, download_name='images')
    except Exception as e:
        return str(e)


if __name__ == "__main__":
    app.run(host="0.0.0.0", debug=False)
