import matplotlib.pyplot as plt  # type: ignore
from plate_condition_vision.analyser import PlateConditionAnalyser
import numpy as np  # type: ignore
import cv2  # type: ignore
from os import listdir
from pypylon import pylon  # type: ignore
import toml


def update_reference(n_imgs: int = 2, include_old: bool = True) -> dict:
    '''update the reference image saved in img_pattern.
    the id is automatically detected by datamatrix. If no datamatrix is detactable,
    id = 0 is used.

    Parameters
    ----------
    n_imgs : int, optional
        number of images, by default 2
    include_old : bool, optional
        use also previousold reference image , by default True

    Returns
    -------
    dict
        "id": int, unique identifier of the build plate
        "n_imgs": int, number of images taken
        "stds_new": the average standard deviation of the new taken pictures over all pixels
        "stds_all": the average standard deviation of all pictures (including the previous ref)
                    over all pixels
        "weights": the weights of the weighted average used to calculate new ref
    '''
    # load config
    with open("config/config.toml") as fh:
        config = toml.load(fh)

    # conecting to the first available camera
    camera = pylon.InstantCamera(pylon.TlFactory.GetInstance().CreateFirstDevice())

    # Grabing Continusely (video) with minimal delay
    camera.StartGrabbingMax(n_imgs)
    converter = pylon.ImageFormatConverter()
    imgs = []

    # converting to opencv bgr format
    converter.OutputPixelFormat = pylon.PixelType_RGB8packed
    converter.OutputBitAlignment = pylon.OutputBitAlignment_MsbAligned
    while camera.IsGrabbing():
        grabResult = camera.RetrieveResult(5000, pylon.TimeoutHandling_ThrowException)

        if grabResult.GrabSucceeded():
            # Access the image data
            image = converter.Convert(grabResult)
            imgs.append(cv2.cvtColor(image.GetArray(), cv2.COLOR_BGR2GRAY))

        grabResult.Release()

    # Releasing the resource
    camera.StopGrabbing()
    camera.Close()

    # caclulate std of new images
    std_new: float = np.mean(np.std(np.array(imgs), axis=0))

    # determine datamatrix id
    pca = PlateConditionAnalyser(imgs[0])
    img_datamatrix = pca.crop_image(
        imgs[0],
        config["rois"]["datamatrix"]["start"],
        config["rois"]["datamatrix"]["end"],
        config["rois"]["datamatrix"]["color"])
    identifier = pca.analyse_datamatrix(img_datamatrix)

    # load and append old ref image if existing and desired
    path_folder = "img_pattern/"
    path_file = f"ref_{str(identifier)}.png"
    consider_old = False
    if path_file in listdir(path_folder) and include_old:
        img_old = cv2.imread(path_folder+path_file)
        ref_old = cv2.cvtColor(img_old, cv2.COLOR_BGR2GRAY)
        imgs.append(ref_old)
        consider_old = True

    imgs_stacked = np.array(imgs)

    # calculate std of all images
    stds: np.ndarray = np.std(imgs_stacked, axis=0)
    std_all: float = np.mean(np.std(imgs_stacked, axis=0))

    # calculate weighted average
    if consider_old:
        w = np.ones(imgs_stacked.shape[0])/(len(imgs_stacked)-1)
        w[-1] = 1
    else:
        w = np.ones(imgs_stacked.shape[0])/(len(imgs_stacked))

    img_avg: np.ndarray = np.average(imgs_stacked, axis=0, weights=w)

    # visualize
    if False:
        _, axs = plt.subplots(1, 3, figsize=(10, 3))
        axs[0].imshow(img_avg, cmap="gray")
        stds_h = axs[1].imshow(stds, cmap="gray", vmin=0, vmax=5)
        plt.colorbar(stds_h, axs[2])
        plt.savefig("test.png")

    # save new image
    cv2.imwrite(path_folder+path_file, img_avg)

    return {"id": int(identifier),
            "n_imgs": int(n_imgs),
            "used_old": str(consider_old),
            "stds_new": float(std_new),
            "stds_all": float(std_all),
            "weights": list(w)}


if __name__ == "__main__":
    print(update_reference(n_imgs=5))
