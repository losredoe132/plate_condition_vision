import time  # type: ignore

import numpy as np  # type: ignore
import RPi.GPIO as GPIO  # type: ignore

import datetime
import logging


logging.basicConfig(filename='pin_state.log', level=logging.DEBUG)

pin_channels = [17, 18]
delta_t = 0.01  # s

GPIO.setmode(GPIO.BCM)
for pin_channel in pin_channels:
    logging.debug(f"channel {pin_channel} is set up.")
    GPIO.setup(pin_channel, GPIO.IN)

vals_old = np.ones_like(pin_channels)

try:
    logging.info("starting while true loop")
    while True:
        for i, pin_channel in enumerate(pin_channels):
            state = GPIO.input(pin_channel)
            if state != vals_old[i]:
                ct = datetime.datetime.now()
                logging.info(f"{ct} state change @ pin {pin_channel}. New state: {state}")
                vals_old[i] = state
            time.sleep(delta_t)

except KeyboardInterrupt:

    logging.info("Press Ctrl-C to terminate while statement")
    GPIO.cleanup()
    logging.debug("everthing is cleaned up for you.")
    pass
