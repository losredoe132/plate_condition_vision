import os
from plate_condition_vision.image_grabber import ImageGrabber

path_config_cam: str = os.path.join(
    os.getcwd(), 'config/acA1920-25uc_23339551.pfs')


ig = ImageGrabber()

ig.load_config_cam(path_config_cam)
