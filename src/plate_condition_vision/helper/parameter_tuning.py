from plate_condition_vision.analyser import PlateConditionAnalyser
from os import listdir
import matplotlib.pyplot as plt  # type: ignore
import logging
from sklearn.metrics import ConfusionMatrixDisplay, accuracy_score  # type: ignore

logging.basicConfig(filename='logs/parameter_tuning.log', level=logging.INFO)

path_img_folder = "img/"
paths = listdir(path_img_folder)
paths.remove("analysis.png")


def determine_target(path):
    if "empty" in path:
        return 0
    else:
        return 1


thresholds = [5, 10, 15, 17, 20, 50]
fig, axs = plt.subplots(len(thresholds), 1, figsize=(10, 10))

for ax, threshold in zip(axs, thresholds):
    targets = []
    actuals = []
    logging.info(f"threshold: {threshold}")
    for path in paths:
        logging.info(f"   path: {path}")
        pca = PlateConditionAnalyser(path_img_folder+path)
        setattr(pca, "threshold_pollution", threshold)
        analysis = pca.analyse()

        actual = int(bool(analysis["pollution_bed"]))
        target = determine_target(path)

        actuals.append(actual)
        targets.append(target)

    acc = accuracy_score(targets, actuals)
    ax.set_title(f"threshold: {threshold} with {acc:.2f}")
    ConfusionMatrixDisplay.from_predictions(targets, actuals, labels=[0, 1], ax=ax, cmap="Greys")

plt.tight_layout()
plt.savefig("conf_matrix.png")
