
import cv2  # type: ignore
import numpy as np  # type: ignore

path = "img_pattern/ref_1.png"
img = cv2.imread(path)
pattern = img[800:870, 1230:1280]
# pattern = img
kernel_size = 2
kernel = np.ones((kernel_size, kernel_size), np.float32)/kernel_size**2
img = cv2.cvtColor(pattern, cv2.COLOR_BGR2GRAY)
img = cv2.filter2D(img, -1, kernel)
cv2.imwrite("img_pattern/pattern_hole.png", img)
